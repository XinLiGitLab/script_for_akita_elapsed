import subprocess
import re

def main():
    for n in range(1, 41):
        proc = subprocess.Popen(
            'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/fir/fir -length=1024 -timing -parallel' % n, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)
        out, err = proc.communicate()
        matches = re.findall(r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out, re.DOTALL)
        print(matches)



if __name__ == "__main__":
    main()
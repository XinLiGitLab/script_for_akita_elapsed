import subprocess
import re
import numpy as np
import pandas as pd

def main():

    #to initialize an array to store the values of the loops into
    elapsed_time_array = np.array([])
    num_cores_array = np.array([])

    #loop for the number of cores
    for num_cores in range(1, 4):

        #python running the simulator
        proc = subprocess.Popen(
            'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/fir/fir -length=1024 -timing -parallel' % num_cores, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

        #out is the varible to store the result of each simulation
        out, err = proc.communicate()

        #Elapsed_time is the variable to store the Elapsed time data from the results
        elapsed_time = re.findall(r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
        
        #stores the results of the loop into an array
        elapsed_time_array = np.append(elapsed_time_array, elapsed_time)
        #stores the amount of cores used into an array
        num_cores_array = np.append(num_cores_array, num_cores)

    #this is to define the word "elapsed" to use later
    elasped = re.findall(r'Elapsed', out, re.DOTALL)
    
    elasped_wall_clock = re.findall(r'wall clock', out, re.DOTALL)

    df = pd.DataFrame(["fir.", num_cores_array, elapsed_time_array], index = ["Benchmark: ", "# of Cores", elasped_wall_clock])

    print(df)

    df.to_csv('data.csv')

if __name__ == "__main__":
    main()
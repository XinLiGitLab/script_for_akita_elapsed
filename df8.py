import subprocess
import re
import numpy as np
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='stuff')
parser.add_argument('-fir', action='store_true', help='fir benchmark')
parser.add_argument('-matrix_multi', action='store_true', help='matrix multiplication benchmark')
parser.add_argument('-kmeans', action='store_true', help='kmeans benchmark')
args = parser.parse_args()

def fir():

    columns = ['benchmark', 'num_cores', 'num_gpu', 'length', 'elapsed_time', 'kernel_time']
    df = pd.DataFrame(columns=columns)
    
    # variables for the loops
    length_count = [262144]
    num_gpu_count = ['1', '1,2', '1,2,3', '1,2,3,4']

    for length in length_count:
        
        for num_gpu in num_gpu_count:

            for num_cores in range (1, 41):
                # python running the simulator
                proc = subprocess.Popen(
                    'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/fir/fir -length=%g -timing -parallel -gpus=%s' % (num_cores, length, num_gpu.strip('"\'')),
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

                # out is the varible to store the result of each simulation
                out, err = proc.communicate()

                # Elapsed_time is the variable to store the Elapsed time data from the results
                elapsed_time = re.findall(
                    r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
                
                # kernel time
                kernel_time = re.findall(
                    r'Kernel time: ([0-9\.:]+)', out)
                
                print("Core #: ", num_cores)

                print(out)
                
                entry = {
                    'benchmark': 'fir',
                    'num_cores': num_cores,
                    'num_gpu': num_gpu,
                    'length': length,
                    'elapsed_time': elapsed_time,
                    'kernel_time': kernel_time
                }
                df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('firtestinggg.csv')


def maxtrix_multi():

    columns = ['benchmark', 'num_cores', 'num_gpu', 'elapsed_time', 'kernel_time', 'total_time']
    df = pd.DataFrame(columns=columns)

    num_gpu_count = ['1', '1,2', '1,2,3', '1,2,3,4']

    # loop for the number of cores
   
    for num_gpu in num_gpu_count:

        for num_cores in range(1, 41):
        
            # python running the simulator
            proc = subprocess.Popen(
                'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/matrixmultiplication/matrixmultiplication -x 512 -y 512 -z 512 -timing -parallel -gpus=%s' % (num_cores, num_gpu.strip('"\'')),
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

            # out is the varible to store the result of each simulation
            out, err = proc.communicate()

            # regular expressions

            elapsed_time = re.findall(
                    r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
                
            kernel_time = re.findall(
                r'Kernel time: ([0-9\.:]+)', out)
            
            total_time = re.findall(
                r'Total time: ([0-9\.:]+)', out)

            print("Core #: ", num_cores)

            print(out)

            entry = {
                'benchmark': 'matrixmultiplication',
                'num_cores': num_cores,
                'num_gpu': num_gpu,
                'elapsed_time': elapsed_time,
                'kernel_time': kernel_time,
                'total_time': total_time,
            }
            df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('matrixmulti_128.csv')

def kmeans():

    columns = ['benchmark', 'num_cores', 'num_gpu', 'elapsed_time', 'kernel_time', 'total_time']
    df = pd.DataFrame(columns=columns)

    num_gpu_count = ['1', '1,2', '1,2,3', '1,2,3,4']
    #num_points_count = [131072, 262144]
    num_points_count = [32768, 65536]

    # loop for the number of cores
    for num_points in num_points_count:

        for num_gpu in num_gpu_count:

            for num_cores in range(1, 41):
            
                # python running the simulator
                proc = subprocess.Popen(
                    'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/kmeans/kmeans -points=%g -parallel -gpus=%s' % (num_cores, num_points, num_gpu.strip('"\'')),
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

                # out is the varible to store the result of each simulation
                out, err = proc.communicate()

                # regular expressions

                elapsed_time = re.findall(
                        r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
                    
                kernel_time = re.findall(
                    r'Kernel time: ([0-9\.:]+)', out)
                
                total_time = re.findall(
                    r'Total time: ([0-9\.:]+)', out)


                print("Core #: ", num_cores)

                print(out)

                entry = {
                    'benchmark': 'kmeans',
                    'num_cores': num_cores,
                    'num_gpu': num_gpu,
                    'points': num_points,
                    'elapsed_time': elapsed_time,
                    'kernel_time': kernel_time,
                    'total_time': total_time,
                }
                df = df.append(entry, ignore_index=True)
        
        print(df)
        df.to_csv('kmeans_test3.csv')



if __name__ == "__main__":
    if args.fir:
        fir()
    elif args.matrix_multi:
        maxtrix_multi()
    elif args.kmeans:
        kmeans()


import subprocess
import re
import numpy as np
import pandas as pd


def main():

    columns = ['benchmark', 'num_cores', 'num_gpu', 'length', 'elapsed_time', 'kernel_time']
    df = pd.DataFrame(columns=columns)

    # variables for the loops
    num_cores_count = [1, 2, 3, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40]
    length_count = [32768, 65536, 131072, 262144]
    num_gpu_count = ['1', '1,2', '1,2,3', '1,2,3,4']

    for length in length_count:
        
        for num_gpu in num_gpu_count:

            for num_cores in num_cores_count:
                # python running the simulator
                proc = subprocess.Popen(
                    'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/fir/fir -length=%g -timing -parallel -gpus=%s' % (num_cores, length, num_gpu.strip('"\'')),
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

                # out is the varible to store the result of each simulation
                out, err = proc.communicate()

                # Elapsed_time is the variable to store the Elapsed time data from the results
                elapsed_time = re.findall(
                    r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
                
                # kernel time
                kernel_time = re.findall(
                    r'Kernel time: ([0-9\.:]+)', out)

                print(out)

                entry = {
                    'benchmark': 'fir',
                    'num_cores': num_cores,
                    'num_gpu': num_gpu,
                    'length': length,
                    'elapsed_time': elapsed_time,
                    'kernel_time': kernel_time
                }
                df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('testtest.csv')


if __name__ == "__main__":
    main()

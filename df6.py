import subprocess
import re
import numpy as np
import pandas as pd


def main():

    columns = ['benchmark', 'num_cores', 'kernel_time', 'total_time', 'GPU_1_kernel_time', 'GPU_2_kernel_time', 'GPU_3_kernel_time', 'GPU_4_kernel_time']
    df = pd.DataFrame(columns=columns)

    # loop for the number of cores
    for num_cores in range(1, 5):

        # python running the simulator
        proc = subprocess.Popen(
            'GOMAXPROCS=%d ~/akita/gcn3/samples/matrixmultiplication/matrixmultiplication -x 256 -y 256 -z 256 -timing -parallel -gpus=1,2,3,4' % num_cores,
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

        # out is the varible to store the result of each simulation
        out, err = proc.communicate()

        # regular expressions
        kernel_time = re.findall(
            r'Elapsed Kernel time: ([0-9\.:]+)', out)
        
        total_time = re.findall(
            r'Total time: ([0-9\.:]+)', out)

        GPU_1_kernel_time = re.findall(
            r'GPU 1 kernel time: ([0-9\.:]+)', out)

        GPU_2_kernel_time = re.findall(
            r'GPU 2 kernel time: ([0-9\.:]+)', out)

        GPU_3_kernel_time = re.findall(
            r'GPU 3 kernel time: ([0-9\.:]+)', out)

        GPU_4_kernel_time = re.findall(
            r'GPU 4 kernel time: ([0-9\.:]+)', out)

        print(out)

        entry = {
            'benchmark': 'matrixmultiplication',
            'num_cores': num_cores,
            'kernel_time': kernel_time,
            'total_time': total_time,
            'GPU_1_kernel_time': GPU_1_kernel_time,
            'GPU_2_kernel_time': GPU_2_kernel_time,
            'GPU_3_kernel_time': GPU_3_kernel_time,
            'GPU_4_kernel_time': GPU_4_kernel_time
        }
        df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('matrixmulti.csv')


if __name__ == "__main__":
    main()

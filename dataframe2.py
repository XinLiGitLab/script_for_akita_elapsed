import subprocess
import re
import numpy as np
import pandas as pd


def main():

    columns = ['benchmark', 'num_cores', 'elapsed_time']
    df = pd.DataFrame(columns=columns)

    # loop for the number of cores
    for num_cores in range(1, 41):

        # python running the simulator
        proc = subprocess.Popen(
            'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/fir/fir -length=1048576 -timing -parallel -gpus=1,2,3,4' % num_cores,
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

        # out is the varible to store the result of each simulation
        out, err = proc.communicate()

        # Elapsed_time is the variable to store the Elapsed time data from the results
        elapsed_time = re.findall(
            r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
        
        print(out)

        entry = {
            'benchmark': 'fir',
            'num_cores': num_cores,
            'elapsed_time': elapsed_time,
        }
        df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('data_1048576_gpus=1,2,3,4.csv')


if __name__ == "__main__":
    main()

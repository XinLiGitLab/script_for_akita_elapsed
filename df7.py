import subprocess
import re
import numpy as np
import pandas as pd


def maxtrix_multi():

    columns = ['benchmark', 'num_cores', 'num_gpu', 'elapsed_time', 'kernel_time', 'total_time']
    df = pd.DataFrame(columns=columns)

    num_gpu_count = ['1', '1,2', '1,2,3', '1,2,3,4']

    # loop for the number of cores
   
    for num_gpu in num_gpu_count:

        for num_cores in range(1, 41):
        
            # python running the simulator
            proc = subprocess.Popen(
                'GOMAXPROCS=%d /usr/bin/time -v ~/akita/gcn3/samples/matrixmultiplication/matrixmultiplication -x 512 -y 512 -z 512 -timing -parallel -gpus=%s' % (num_cores, num_gpu.strip('"\'')),
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

            # out is the varible to store the result of each simulation
            out, err = proc.communicate()

            # regular expressions

            elapsed_time = re.findall(
                    r'Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): ([0-9\.:]+)', out)
                
            kernel_time = re.findall(
                r'Kernel time: ([0-9\.:]+)', out)
            
            total_time = re.findall(
                r'Total time: ([0-9\.:]+)', out)

            print(out)

            entry = {
                'benchmark': 'matrixmultiplication',
                'num_cores': num_cores,
                'num_gpu': num_gpu,
                'elapsed_time': elapsed_time,
                'kernel_time': kernel_time,
                'total_time': total_time,
            }
            df = df.append(entry, ignore_index=True)
    
    print(df)
    df.to_csv('matrixmulti_test2.csv')


if __name__ == "__main__":
    maxtrix_multi()
